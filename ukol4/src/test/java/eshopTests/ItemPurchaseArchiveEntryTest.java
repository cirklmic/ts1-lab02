package eshopTests;

import cz.cvut.fel.ts1.archive.ItemPurchaseArchiveEntry;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ItemPurchaseArchiveEntryTest {
    private Item item = new StandardItem(55, "Banana", 6, "Fruit", 10);

    @Test
    public void constructorItem() {
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        Assertions.assertEquals(item, ipae.getRefItem());
    }

    @Test
    public void constructorCount() {
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        Assertions.assertEquals(1, ipae.getCountHowManyTimesHasBeenSold());
    }
}
