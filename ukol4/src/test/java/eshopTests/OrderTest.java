package eshopTests;

import cz.cvut.fel.ts1.shop.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;

public class OrderTest {
    private String sampleName = "Jan";
    private String sampleCustomerAddress = "Pod Mostem 22";
    private int sampleState = 1;

    @Test
    public void constructorWithStateName() {
        Order order = new Order(new ShoppingCart(), sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(sampleName, order.getCustomerName());
    }

    @Test
    public void constructorWithStateAddress() {
        Order order = new Order(new ShoppingCart(), sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(sampleCustomerAddress, order.getCustomerAddress());
    }

    @Test
    public void constructorWithStateState() {
        Order order = new Order(new ShoppingCart(), sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(sampleState, order.getState());
    }

    @Test
    public void constructorWithStateCartEmpty() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }
    @Test
    public void constructorWithStateCartStandardItem() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new StandardItem(55, "Banana", 6, "Fruit", 10));
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void constructorWithStateCartDiscountedItem() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new DiscountedItem(55, "Banana", 6, "Fruit", 20, new Date(124, 3, 1),  new Date(124, 3, 20)));
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, sampleName, sampleCustomerAddress, sampleState);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void constructorWithoutStateName() {
        Order order = new Order(new ShoppingCart(), sampleName, sampleCustomerAddress);
        Assertions.assertEquals(sampleName, order.getCustomerName());
    }

    @Test
    public void constructorWithoutStateAddress() {
        Order order = new Order(new ShoppingCart(), sampleName, sampleCustomerAddress);
        Assertions.assertEquals(sampleCustomerAddress, order.getCustomerAddress());
    }

    @Test
    public void constructorWithoutStateCartEmpty() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, sampleName, sampleCustomerAddress);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }
    @Test
    public void constructorWithoutStateCartStandardItem() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new StandardItem(55, "Banana", 6, "Fruit", 10));
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, sampleName, sampleCustomerAddress);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void constructorWithoutStateCartDiscountedItem() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new DiscountedItem(55, "Banana", 6, "Fruit", 20, new Date(124, 3, 1),  new Date(124, 3, 20)));
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, sampleName, sampleCustomerAddress);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
    }

    @Test
    public void constructorWithStateCartNullCart() {
        Assertions.assertThrows(NullPointerException.class, () -> new Order(null, sampleName, sampleCustomerAddress, sampleState));
    }

    @Test
    public void constructorWithoutStateCartNullCart() {
        Assertions.assertThrows(NullPointerException.class, () -> new Order(null, sampleName, sampleCustomerAddress));
    }
}
