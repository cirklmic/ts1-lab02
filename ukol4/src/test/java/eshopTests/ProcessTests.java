package eshopTests;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.*;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static cz.cvut.fel.ts1.shop.EShopController.*;


public class ProcessTests {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final PrintStream originalOut = System.out;

    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setupStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @AfterEach
    public void cleanData() {
        storage = null;
    }
    @Test
    public void notEnoughStock() {
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();
        Assertions.assertEquals(EShopController.carts.size(), 1);

        int[] itemCount = {10,10,4,5,10,2};
        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }

        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[4]);
        newCart.addItem(storageItems[5]);
        newCart.addItem(storageItems[5]);
        newCart.addItem(storageItems[5]);
        //not enough items when purchasing
        Assertions.assertThrows(NoItemInStorage.class, () -> purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8"));

        //Item exists, but not in storage
        Item unstoredItem =  new DiscountedItem(8, "Toilet paper roll", 50, "GADGETS", 10, "1.8.2014", "1.12.2014");
        Assertions.assertThrows(NoItemInStorage.class, () -> storage.removeItems(unstoredItem, 1));

        //Item is in storage, but not enough of it
        Item storedItem = new StandardItem(10, "Plush banana for scale", 100, "GADGETS", 5);
        storage.insertItems(storedItem, 2);
        Assertions.assertThrows(NoItemInStorage.class, () -> storage.removeItems(unstoredItem, 3));

        ShoppingCart bananaCart = EShopController.newCart();
        Assertions.assertEquals(EShopController.carts.size(), 2);
        bananaCart.addItem(storedItem);
        bananaCart.addItem(storedItem);
        bananaCart.addItem(storedItem);
        Assertions.assertThrows(NoItemInStorage.class, () -> purchaseShoppingCart(bananaCart, "Jan Novák","Kosmonautu 25, Praha 8"));
    }

    @Test
    public void buyItems() {
        //testing: add items to cart, remove, archive
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();

        int[] itemCount = {10,10,4,5,10,2};
        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> newCart.removeItem(294583985));

        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.removeItem(storageItems[1].getID());

        //archive
        archive.putOrderToPurchasesArchive(new Order(newCart, "já", "pod mostem 22"));
        Assertions.assertEquals(2, archive.getHowManyTimesHasBeenItemSold(storageItems[0]));
        archive.printItemPurchaseStatistics();
        Assertions.assertEquals("""
                Item with ID 1 added to the shopping cart.
                Item with ID 1 added to the shopping cart.
                Item with ID 2 added to the shopping cart.
                ITEM PURCHASE STATISTICS:
                ITEM  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0   LOYALTY POINTS 5   HAS BEEN SOLD 2 TIMES
                ITEM  Item   ID 2   NAME Dancing Panda v.3 with USB port   CATEGORY GADGETS   PRICE 6000.0   LOYALTY POINTS 10   HAS BEEN SOLD 1 TIMES
                """, outContent.toString().replaceAll("\\r\\n?", "\n"));

       Assertions.assertDoesNotThrow(() ->  EShopController.purchaseShoppingCart(newCart, "Lucie", "Hloubětín"));
    }
}
