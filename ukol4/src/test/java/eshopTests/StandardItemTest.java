package eshopTests;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StandardItemTest {
    private StandardItem sd;
    private int sampleID;
    private String sampleName;
    private float samplePrice;
    private String sampleCategory;
    private int sampleLoyaltyPoints;
    @BeforeEach
    public void initItem() {
        sampleID = 55;
        sampleName = "Banana";
        samplePrice = 6;
        sampleCategory = "Fruit";
        sampleLoyaltyPoints = 10;
        sd = new StandardItem(sampleID, sampleName, samplePrice, sampleCategory, sampleLoyaltyPoints);
    }
    @Test
    public void constructorID() {
        Assertions.assertEquals(sampleID, sd.getID());
    }

    @Test
    public void constructorName() {
        Assertions.assertEquals(sampleName, sd.getName());
    }

    @Test
    public void constructorPrice() {
        Assertions.assertEquals(samplePrice, sd.getPrice());
    }

    @Test
    public void constructorCategory() {
        Assertions.assertEquals(sampleCategory, sd.getCategory());
    }

    @Test
    public void constructorLoyaltyPoints() {
        Assertions.assertEquals(sampleLoyaltyPoints, sd.getLoyaltyPoints());
    }

    @Test
    public void copyID() {
        StandardItem copied = sd.copy();
        Assertions.assertEquals(copied.getID(), sd.getID());
    }

    @Test
    public void copyName() {
        StandardItem copied = sd.copy();
        Assertions.assertEquals(copied.getName(), sd.getName());
    }

    @Test
    public void copyPrice() {
        StandardItem copied = sd.copy();
        Assertions.assertEquals(copied.getPrice(), sd.getPrice());
    }

    @Test
    public void copyCategory() {
        StandardItem copied = sd.copy();
        Assertions.assertEquals(copied.getCategory(), sd.getCategory());
    }

    @Test
    public void copyLoyaltyPoints() {
        StandardItem copied = sd.copy();
        Assertions.assertEquals(copied.getLoyaltyPoints(), sd.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "55, 'Banana', 6, 'Fruit', 10, 55, 'Banana', 6, 'Fruit', 10, true",
            "55, 'Banana', 6, 'Fruit', 10, 55, 'Orange', 6, 'Fruit', 10, false",
            "55, 'Banana', 6, 'Vegetable', 10, 55, 'Banana', 6, 'Fruit', 10, false",
            "52, 'Banana', 6, 'Fruit', 10, 23, 'Banana', 6, 'Fruit', 10, false",

    })
    public void equals(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                       int id2, String name2, float price2, String category2, int loyaltyPoints2,
                       boolean expected) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        Assertions.assertEquals(expected, item1.equals(item2));
    }


}
