package eshopTests;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;

public class PurchasesArchiveTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final PrintStream originalOut = System.out;

    private final PrintStream originalErr = System.err;

    private PurchasesArchive pa;

    private Order order;

    private Item item = new StandardItem(55, "Banana", 6, "Fruit", 10);

    PurchasesArchive par = mock(PurchasesArchive.class);
    ItemPurchaseArchiveEntryTest archiveEntryMock = mock(ItemPurchaseArchiveEntryTest.class);

    @BeforeEach
    public void setupStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @BeforeEach
    public void initData() {
        pa = new PurchasesArchive();
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        String sampleName = "Jan";
        String sampleCustomerAddress = "Pod Mostem 22";
        int sampleState = 1;
        order = new Order(new ShoppingCart(items), sampleName, sampleCustomerAddress, sampleState);
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void printItemPurchaseStatisticsEmpty() {
        pa.printItemPurchaseStatistics();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:\r\n", outContent.toString());
    }

    @Test
    public void printItemPurchaseStatisticsNotEmpty() {
        pa.putOrderToPurchasesArchive(order);
        pa.printItemPurchaseStatistics();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:\r\nITEM  Item   ID 55   NAME Banana   CATEGORY Fruit   PRICE 6.0   LOYALTY POINTS 10   HAS BEEN SOLD 1 TIMES\r\n", outContent.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold() {
        Assertions.assertEquals(pa.getHowManyTimesHasBeenItemSold(new StandardItem(55, "Banana", 6, "Fruit", 10)), 0);
    }

    @Test
    public void putOrderToPurchasesArchive() {
        pa.putOrderToPurchasesArchive(order);
        Assertions.assertEquals(pa.getHowManyTimesHasBeenItemSold(item), 1);
    }
}
