package eshopTests;

import cz.cvut.fel.ts1.shop.DiscountedItem;
import cz.cvut.fel.ts1.shop.StandardItem;
import cz.cvut.fel.ts1.storage.ItemStock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Date;

public class ItemStockTest {
    private StandardItem sd;
    private DiscountedItem di;
    private int sampleID;
    private String sampleName;
    private float samplePrice;
    private String sampleCategory;
    private int sampleLoyaltyPoints;
    private int discount;
    private Date from;
    private Date to;

    @BeforeEach
    public void initData() {
        sampleID = 55;
        sampleName = "Banana";
        samplePrice = 6;
        sampleCategory = "Fruit";
        sampleLoyaltyPoints = 10;
        discount = 10;
        from = new Date();
        to = new Date(from.getTime() + (1000*60*60)); // hour after
        sd = new StandardItem(sampleID, sampleName, samplePrice, sampleCategory, sampleLoyaltyPoints);
        di = new DiscountedItem(sampleID, sampleName, samplePrice, sampleCategory, discount, from, to);
    }
    @Test
    public void constructorCountStandardItem() {
        ItemStock is = new ItemStock(sd);
        Assertions.assertEquals(is.getCount(), 0);
    }
    @Test
    public void constructorCountDiscountedItem() {
        ItemStock is = new ItemStock(di);
        Assertions.assertEquals(is.getCount(), 0);
    }

    @Test
    public void constructorRefItem() {
        ItemStock is = new ItemStock(sd);
        Assertions.assertEquals(sd, is.getItem());
    }

    @ParameterizedTest()
    @CsvSource({"1","2","60","-1","343"})
    public void incrementCountStandard(int x) {
        ItemStock is = new ItemStock(sd);
        int initialCount = is.getCount();
        is.increaseItemCount(x);

        Assertions.assertEquals(initialCount + x, is.getCount());
    }

    @ParameterizedTest()
    @CsvSource({"1","2","60","-1","343"})
    public void incrementCountDiscounted(int x) {
        ItemStock is = new ItemStock(di);
        int initialCount = is.getCount();
        is.increaseItemCount(x);

        Assertions.assertEquals(initialCount + x, is.getCount());
    }

    @ParameterizedTest()
    @CsvSource({"1","2","60","-1","343"})
    public void decrementCountStandard(int x) {
        ItemStock is = new ItemStock(sd);
        int initialCount = is.getCount();
        is.decreaseItemCount(x);

        Assertions.assertEquals(initialCount - x, is.getCount());
    }

    @ParameterizedTest()
    @CsvSource({"1","2","60","-1","343"})
    public void decrementCountDiscounted(int x) {
        ItemStock is = new ItemStock(di);
        int initialCount = is.getCount();
        is.decreaseItemCount(x);

        Assertions.assertEquals(initialCount - x, is.getCount());
    }


}
