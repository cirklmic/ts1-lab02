import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class SeleniumRobotTest {
    @Test
    public void moodleTest() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://moodle.fel.cvut.cz/mod/quiz/view.php?id=308513");
        Thread.sleep(15000); //waits for me to log in
        driver.findElement(By.cssSelector("form button")).click(); //start quiz
        driver.findElement(By.cssSelector("#id_submitbutton")).click(); //confirm
        driver.findElement(By.cssSelector(".answer textarea")).sendKeys("Michael Cirkl, paralelka 108");
        driver.findElement(By.cssSelector(".answer input")).sendKeys("86400");
        Select planetBySelect = new Select(driver.findElement(By.cssSelector("form > div > div:nth-child(3) select")));
        planetBySelect.selectByVisibleText("Oberon"); //select v divu pro 3. otázku
        Select countryBySelect = new Select(driver.findElement(By.cssSelector("form > div > div:nth-child(4) select")));
        countryBySelect.selectByVisibleText("Rumunsko"); //select v divu pro 4. otázku
        driver.findElement(By.cssSelector("#mod_quiz-next-nav")).click(); //odevzdání
        driver.quit();
    }
}
