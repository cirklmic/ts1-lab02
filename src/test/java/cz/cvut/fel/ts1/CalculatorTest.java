package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    public void initData() {
        calculator = new Calculator();
    }

    @Test
    public void addRegular() {
        Assertions.assertEquals(12, calculator.add(5, 7));
    }
    @Test
    public void subtractRegular() {
        Assertions.assertEquals(13, calculator.subtract(24, 11));
    }

    @Test
    public void multiplyRegular() {
        Assertions.assertEquals(42, calculator.multiply(6, 7));
    }

    @Test
    public void divideRegular() {
        Assertions.assertEquals(3, calculator.divide(51, 17));
    }

    @Test
    public void divideByZero() {
        Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(5,0));
    }
}