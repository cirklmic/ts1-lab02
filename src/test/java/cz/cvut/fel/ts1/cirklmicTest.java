package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class cirklmicTest {
    @Test
    public void testFactorial5() {
        cirklmic user = new cirklmic();
        assertEquals(120, user.factorial(5));
    }

    @Test
    public void testFactorial0() {
        cirklmic user = new cirklmic();
        assertEquals(1, user.factorial(0));
    }
    @Test
    public void testFactorialNegative() {
        cirklmic user = new cirklmic();
        assertThrows(IllegalArgumentException.class, () -> user.factorial(-10));
    }


    @Test
    public void testRecursiveFactorial5() {
        cirklmic user = new cirklmic();
        assertEquals(120, user.factorialRecursive(5));
    }

    @Test
    public void testRecursiveFactorial0() {
        cirklmic user = new cirklmic();
        assertEquals(1, user.factorialRecursive(0));
    }
    @Test
    public void testRecursiveFactorialNegative() {
        cirklmic user = new cirklmic();
        assertThrows(IllegalArgumentException.class, () -> user.factorialRecursive(-10));
    }
}