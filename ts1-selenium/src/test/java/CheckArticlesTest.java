import POMs.*;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class  CheckArticlesTest {
    private WebDriver driver;
    private AdvancedSearchPage advancedSearchPage;
    private SearchPage searchPage;
    private ArticlePage articlePage;
    private MainPage mainPage;
    private LoginPage loginPage;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        advancedSearchPage = new AdvancedSearchPage(driver);
        searchPage = new SearchPage(driver);
        articlePage = new ArticlePage(driver);
        mainPage = new MainPage(driver);
        loginPage = new LoginPage(driver);
        driver.manage().window().setSize(new Dimension(960, 720));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }


    private List<String[]> readCsv(String filePath) {
        List<String[]> data = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(filePath))) {
            data = csvReader.readAll();
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        return data;
    }

    @ParameterizedTest(name = "Article with name {0} should have doi {1} and date {2}")
    @CsvFileSource(files = "articles.csv")
    public void checkArticles(String articleName, String doi, String articleDate){
        mainPage.openMainPage();
        mainPage.clickAcceptCookiesButton();
        mainPage.clickLoginButton();
        //now is at login page
        loginPage.clickAcceptCookiesButton();
        loginPage.enterEmail("xerawil841@javnoi.com"); //tempmail, don't worry, not mine
        loginPage.clickConfirmEmail();
        loginPage.enterPassword("exampleheslo123");
        loginPage.clickConfirmPassword();

        advancedSearchPage.openAdvancedSearchPage();
        advancedSearchPage.enterAllWords(articleName);
        advancedSearchPage.clickSubmitSearchButton();
        //on search page
        searchPage.clickOnNthArticle(1);
        //on article page
        Assertions.assertEquals(doi, articlePage.getArticleDOI());
        Assertions.assertEquals(articleDate, articlePage.getArticleDate());
    }
}
