import POMs.AdvancedSearchPage;
import POMs.ArticlePage;
import POMs.SearchPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class SaveArticlesTest {
    private WebDriver driver;
    private AdvancedSearchPage advancedSearchPage;
    private SearchPage searchPage;
    private ArticlePage articlePage;
    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        advancedSearchPage = new AdvancedSearchPage(driver);
        searchPage = new SearchPage(driver);
        articlePage = new ArticlePage(driver);
        driver.manage().window().setSize(new Dimension(960, 720));
    }
    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void saveArticles() {
        advancedSearchPage.openAdvancedSearchPage();
        advancedSearchPage.clickAcceptCookiesButton();
        advancedSearchPage.enterAllWords("Page Object Model");
        advancedSearchPage.enterAtLeastOneOfWords("Selenium Testing");
        advancedSearchPage.enterStartYear("2024");
        advancedSearchPage.enterEndYear("2024");
        advancedSearchPage.clickSubmitSearchButton();

        //now is at search results
        searchPage.clickFiltersButton();
        searchPage.clickArticleFilterButton();
        searchPage.clickUpdateResultsButton();

        ArrayList<String> articleInfo = new ArrayList<String>();

        //get info of first four articles
        for (int i = 1; i <= 4; i++) {
            searchPage.clickOnNthArticle(i);
            articleInfo.add(articlePage.getArticleTitle());
            articleInfo.add(articlePage.getArticleDOI());
            articleInfo.add(articlePage.getArticleDate());
            driver.navigate().back();
        }
        //writes a csv file
        saveToCSV(articleInfo, "articles.csv");
    }
    private void saveToCSV(ArrayList<String> data, String filePath) {
        String resourcesPath = Paths.get(filePath).toString();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(resourcesPath))) {
            for (int i = 0; i < data.size(); i += 3) {
                StringBuilder line = new StringBuilder();

                for (int j = 0; j < 3; j++) {
                    if (i + j < data.size()) {
                        line.append(data.get(i + j));
                        if (j < 2) {
                            line.append(",");
                        }
                    }
                }

                writer.write(line.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
