package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlePage {
    private WebDriver driver;

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#main > section > div > div > div.app-article-masthead__info > h1")
    private WebElement articleTitle;

    @FindBy(css = ".c-article-identifiers__item > time")
    private WebElement articleDate;

    @FindBy(css = "#article-info-content > div > div:nth-child(2) > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--full-width > p > span.c-bibliographic-information__value")
    private WebElement doi;

    public String getArticleTitle() {
        return articleTitle.getText();
    }

    public String getArticleDate() {
        return articleDate.getText();
    }

    public String getArticleDOI() {
        return doi.getText();
    }
}
