package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#identity-account-widget > span")
    private WebElement loginButton;

    @FindBy(css = "body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept")
    private WebElement acceptCookiesButton;

    public void clickLoginButton() {
        loginButton.click();
    }

    public void clickAcceptCookiesButton() {
        acceptCookiesButton.click();
    }

    public void openMainPage() {
        driver.get("https://link.springer.com/");
    }
}
