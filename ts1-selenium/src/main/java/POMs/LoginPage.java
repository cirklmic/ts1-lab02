package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "login-email")
    private WebElement emailField;

    @FindBy(id = "email-submit")
    private WebElement emailContinueButton;

    @FindBy(id = "login-password")
    private WebElement passwordField;

    @FindBy(id = "password-submit")
    private WebElement continuePasswordButton;

    @FindBy(css = "body > section > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-accept")
    private WebElement acceptCookiesButton;

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void clickConfirmEmail() {
        emailContinueButton.click();
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickConfirmPassword() {
        continuePasswordButton.click();
    }

    public void clickAcceptCookiesButton() {
        acceptCookiesButton.click();
    }
}
