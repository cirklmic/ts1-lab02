package POMs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvancedSearchPage {
    private WebDriver driver;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "all-words")
    private WebElement allWords;

    @FindBy(id = "least-words")
    private WebElement atLeastOneOfWords;

    @FindBy(id = "facet-start-year")
    private WebElement startYear;

    @FindBy(id = "facet-end-year")
    private WebElement endYear;

    @FindBy(id = "submit-advanced-search")
    private WebElement submitSearchButton;

    @FindBy(css = "body > section > div > div.cc-banner__footer > button.cc-button.cc-button--contrast.cc-banner__button.cc-banner__button-accept")
    private WebElement acceptCookiesButton;

    public void enterAllWords(String words) {
        allWords.sendKeys(words);
    }

    public void enterAtLeastOneOfWords(String words) {
        atLeastOneOfWords.sendKeys(words);
    }

    public void enterStartYear(String startingYear) {
        startYear.sendKeys(startingYear);
    }

    public void enterEndYear(String endingYear) {
        endYear.sendKeys(endingYear);
    }

    public void clickSubmitSearchButton() {
        submitSearchButton.click();
    }

    public void clickAcceptCookiesButton() {
        acceptCookiesButton.click();
    }

    public void openAdvancedSearchPage() {
        driver.get("https://link.springer.com/advanced-search");
    }
}
