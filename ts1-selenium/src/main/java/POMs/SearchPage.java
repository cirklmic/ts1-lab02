package POMs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {
    private WebDriver driver;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#main > div > div:nth-child(4) > div > div:nth-child(2) > div.app-search__filter-sort > div.app-search__filter-btn > div > button")
    private WebElement filtersButton;

    @FindBy(css = "#list-content-type-filter > li:nth-child(2) > div > label")
    private WebElement articleFilterButton;

    @FindBy(css = "#popup-filters > div.app-filter-buttons > button.eds-c-button.eds-c-button--primary")
    private WebElement updateResultsButton;

    public void clickArticleFilterButton() {
        articleFilterButton.click();
    }

    public void clickFiltersButton() {
        filtersButton.click();
    }

    public void clickUpdateResultsButton() {
        updateResultsButton.click();
    }

    public void clickOnNthArticle(int n) {
        String selector = String.format("#main > div > div:nth-child(4) > div > div:nth-child(2) > div:nth-child(2) > ol > li:nth-child(%d)", n);
        driver.findElement(By.cssSelector(selector)).click();
    }
}
